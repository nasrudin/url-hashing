## TODO

-   Nosql database to be used instead of rdbms to improve on scale.
-   Hashing to be used if required right now the system randomly generates a shortcode and maps it to the original url.
I did not use encryption as the encryption was resulting in long encrypted strings. To keep the url short I have used shotcode mapping logic.
-   Proper logging to be done on code level.
-   Creating the audit logs on database level to record all kind of operations on database.
-   Recording the server request and response logs in database.
-   Authentication on api access. Right now all api's are open.
-   TestCases to be written. 
-   Improvements tobe done on swagger






