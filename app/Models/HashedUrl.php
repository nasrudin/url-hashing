<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HashedUrl extends Model
{
    use HasFactory;
    protected $appends = ['hashed_url'];
    protected $fillable = [
        'long_url',
        'short_code'
    ];

    public function getHashedUrlAttribute() {
        return env('APP_URL').$this->short_code;
    }
}
