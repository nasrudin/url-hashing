<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
/**
     * @OA\Info(
     *      version="1.0.0",
     *      title="URL HASHING Documentation",
     *      description="A service that encrypts the long url's to make is short and simple for using it in the emails, sheets or documents and also to prevent the url's from being invalid.",
     *      @OA\Contact(
     *          email="nasrudinsha4@gmail.com"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     * 
     *  @OA\Schema(
    *     schema="ResponseDTO",
    *     type="object",
    *     @OA\Property(
    *        property="foo",
    *        type="string"
    *     )
    * )
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Demo API Server"
     * )

     *
     * @OA\Tag(
     *     name="URL HASHING",
     *     description="API Endpoints of url hashing system"
     * )
     */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
