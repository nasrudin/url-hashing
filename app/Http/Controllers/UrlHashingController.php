<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUrlRequest;
use App\Models\HashedUrl;
use App\Services\UrlHashingService;
class UrlHashingController extends Controller
{
    /**
     * @OA\Get(
     *      path="/{short_code}",
     *      operationId="Visit the original link by clicking the short url",
     *      tags={"Short link to be used in browser"},
     *      summary="Visit the original link by clicking the short url(brower link)",
     *      description="This is a browser link not to be consumed as api",
     *      @OA\Parameter(
     *          name="short_code",
     *          description="Short Code",
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function index($short_code)
    {
        $hashedUrl = HashedUrl::where('hits','<',env('LINK_VIEW_LIMIT'))->where('short_code',$short_code)->first();
        if (empty($hashedUrl)) 
            return response()->json(["error" => false, "message" => "URL expired or not found"], 200);
        $hashedUrl->hits = $hashedUrl->hits + 1;
        $hashedUrl->save();   
        return redirect()->away($hashedUrl->long_url);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @OA\Post(
     *      path="/api/v1/url-hashing",
     *      operationId="storeProject",
     *      tags={"Create Short URL"},
     *      summary="Encrypt a long URL and store it in the database",
     *      description="Returns json data",
     *      @OA\Parameter(
     *          name="long_url",
     *          description="Original url to be encrypted",
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     * )
     */
    public function store(StoreUrlRequest $request)
    {
        $hashedUrl = (new UrlHashingService)->storeUrls($request->all());
        return response()->json(["error" => false, "message" => "Success", 'data' => $hashedUrl], 200);
    }

    /**
     * @OA\Get(
     *      path="/api/v1/url-hashing/{short_code}",
     *      operationId="Fetech short url details",
     *      tags={"Get URL details"},
     *      summary="Fetch the details of the short URL",
     *      description="Details of the short URL stored in the database.",
     *      @OA\Parameter(
     *          name="short_code",
     *          description="Short Code",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\JsonContent()
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      )
     *     )
     */
    public function show($short_code)
    {
        $hashedUrl = HashedUrl::where('short_code',$short_code)->first();
        if (empty($hashedUrl)) 
            return response()->json(["error" => false, "message" => "No url found"], 200); 
        return response()->json(["error" => false, "message" => "Success", 'data' => $hashedUrl], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
