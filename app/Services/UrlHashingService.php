<?php

namespace App\Services;

use App\Models\HashedUrl;
class UrlHashingService
{
    /**
     * Create a shortcode a randomly.
     * long url is stored in the batabse
     * @param  int  $length
     * @return string $randString
     */    
    function create_short_code($length = 6)
    {
        $sets = explode('|', env('CHARS'));
        $all = '';
        $randString = '';
        foreach($sets as $set){
            $randString .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++){
            $randString .= $all[array_rand($all)];
        }
        $randString = str_shuffle($randString);
        return $randString;
    }

    /**
     * Create a shortcode and store it along with the long url only if the long url doesnt exists in the databases.
     * long url is stored in the batabse
     * @param  array  $arr
     * @return Model\HashedUrl
     */    
    function storeUrls(array $arr) :HashedUrl {
        try {
            $short_code = $this->create_short_code();
            return HashedUrl::firstOrCreate([
                'long_url' => $arr['long_url']
            ], [
                'short_code' => $short_code,
                'long_url' => $arr['long_url']
            ]);
            
        } catch (\Exception $e){
            //Log errors
            throw new \Exception ($e->getMessage());
        }
    }
}