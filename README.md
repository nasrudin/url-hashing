## Description about your architecture choice

- Having Hands-on experience in PHP and my local system has all the configurations ready for PHP, Hence chose PHP as the programing laguage for this project. Other programing langueages like python or nodejs would take a slight delay in completing the task.
- Chose Laravel PHP framework instead of using core PHP, Codeigniter or CAKEPHP. Laravel gives robust tools for dependency injection, unit testing, queues, real-time events, and its a scalable framework.
- PGSQL, Chose postgres to save time as it was already configured in the system.
