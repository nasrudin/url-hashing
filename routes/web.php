<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function (){
    return response()->json(["error" => false, "message" => "Hello Welcome."], 200);
});
Route::get('/{url}','\App\Http\Controllers\UrlHashingController@index');

