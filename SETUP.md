## Steps to use the application

This micro service enables encryption or shortening of long url's. A new short url is created againts the original long url and  number of hits on the short url is recorded.

## To run the application on local

Clone the project using command.
-   git clone git@gitlab.com:nasrudin/url-hashing.git
-   composer install
Create database in postgres.
-   Database name "url_hashing"
Setup ENV file
-   Copy the content shown in .env.example file to your .env file
-   Configure dabtase details in .env
Run the migrations to create tables in the database. 
-   php artisan migrate:fresh
Run the application.
-   php artisan serve
Test cases to run
-   php artisan test --env=testing

## To deploy the application - Note additional steps may require
-   upload the project on hosting server
-   composer install
-   create env file
-   create database and update database information in the env file
-   create htaccess file if required
-   run migration
-   map domain


