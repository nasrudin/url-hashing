## Assumptions

To build a service which encrypts the long url's to make is short and simple for using it in the emails, sheets or documents and also to prevent the url's from being invalid.

Things to keep in mind: 
URL length can’t be restricted. - no restrictions on length
Query parameters can’t be ignored.  - query parameter will not be ignored
Click tracking should be there but hashed URLs can be made privacy-aware. - number of hits recorded
May be used in a secure manner as the generated URL might be single (limited) use only. - Link is accessible only for 5 times
