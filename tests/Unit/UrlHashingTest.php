<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\UrlHashingService;
class UrlHashingTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_create_short_code()
    {
        $code = (new UrlHashingService)->create_short_code();
        $this->assertNotEmpty($code);
    }

    public function test_encrypt_and_store_url()
    {
        $arr['long_url'] = 'http://www.google.com';
        $HashedUrl = (new UrlHashingService)->storeUrls($arr);
        $this->assertNotEmpty($HashedUrl);
    }
}
